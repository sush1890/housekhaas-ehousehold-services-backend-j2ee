package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Orders")
public class Order {

	private Integer oid;
	@NotEmpty
	private String expectTime;
	@NotEmpty
	private String expectDate;
	private String orderDate;
	@NotEmpty
	private String issue;
	private double amount;
	private String paymentRemark;
	private String status;
	private String address;
	private String mobileNo;
	private String service;
	@JsonManagedReference
	private Professional professional;
	@JsonManagedReference
	private Customer customer;
	
	public Order() {
		System.out.println("in cnstr " + getClass().getName());
	}
	//(address, amount, cid, expect_date, expect_time, issue, mobile_no, order_date, payment_remark, pid, service, status)
	public Order(String address, double amount, String expectDate, String expectTime, String issue, String orderDate, String paymentRemark, 
			String mobileNo, String service, String status) {
		super();
		this.expectTime = expectTime;
		this.expectDate = expectDate;
		this.orderDate = orderDate;
		this.issue = issue;
		this.amount = amount;
		this.paymentRemark = paymentRemark;
		this.status = status;
		this.address = address;
		this.mobileNo = mobileNo;
		this.service = service;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	@Column(name = "mobile_no")
	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}
	
	public String getStatus() {
		return status;
	}

	@Column(name = "order_date")
	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	@Column(name="expect_time")
	public String getExpectTime() {
		return expectTime;
	}

	public void setExpectTime(String expectTime) {
		this.expectTime = expectTime;
	}

	@Column(name="expect_date")
	public String getExpectDate() {
		return expectDate;
	}

	public void setExpectDate(String expectDate) {
		this.expectDate = expectDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "payment_remark")
	public String getPaymentRemark() {
		return paymentRemark;
	}

	public void setPaymentRemark(String paymentRemark) {
		this.paymentRemark = paymentRemark;
	}
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@ManyToOne()
	@JoinColumn(name="pid")
	public Professional getProfessional() {
		return professional;
	}

	public void setProfessional(Professional professional) {
		this.professional = professional;
	}

	@ManyToOne()
	@JoinColumn(name="cid")
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
