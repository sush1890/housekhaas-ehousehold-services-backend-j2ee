package com.app.service;

import java.util.List;

import com.app.pojos.Professional;
import com.app.pojos.Skill;


public interface IProfessionalService {

	String registerProfessional(Professional p);

	List<Skill> getSkills();

	public String validateProfessional(String email, String password);

	List<Professional> getProfessionalList();

	Professional getDetails(int id);

	String deleteProfessional(int id);

	String updateProfessional(Professional p);

}
