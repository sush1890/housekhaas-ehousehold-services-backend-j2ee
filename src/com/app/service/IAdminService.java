package com.app.service;

import java.util.List;

import com.app.pojos.Feedback;
import com.app.pojos.Order;
import com.app.pojos.Skill;

public interface IAdminService {

	String validateAdmin(String email, String password);

	String addService(Skill s);

	List<Feedback> getFeedbackList();

	List<Order> getAllOrders();

	String verifyProfessional(int id);

}
