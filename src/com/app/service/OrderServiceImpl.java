package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IOrderDao;
import com.app.pojos.Order;
import com.app.pojos.ProxyAcceptedOrder;

@Service
@Transactional
public class OrderServiceImpl implements IOrderService {
	
	@Autowired
	private IOrderDao dao;

	public OrderServiceImpl() {
		System.out.println("in order service impl");	
	}

	@Override
	public String bookOrder(int cid, Order o) {
		return dao.bookOrder(cid,o);
	}

	@Override
	public List<Order> orders(int cid) {
		System.out.println("inside order service");
		return dao.orders(cid);
	}

	@Override
	public List<Order> getNewOrders(String skill) {
		return dao.getNewOrders(skill);
	}

	@Override
	public String acceptOrder(int oid, int pid) {
		return dao.acceptOrder(oid,pid);
	}

	@Override
	public List<ProxyAcceptedOrder> acceptedOrders(int pid) {
		return dao.acceptedOrders(pid);
	}

	@Override
	public String orderCompleted(int oid) {
		return dao.orderCompleted(oid);
	}

	@Override
	public String cancelOrder(int oid) {
		return dao.cancelOrder(oid);
	}

}
