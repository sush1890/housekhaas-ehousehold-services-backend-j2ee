package com.app.service;

import java.util.List;

import com.app.pojos.Order;
import com.app.pojos.ProxyAcceptedOrder;

public interface IOrderService {

	String bookOrder(int cid, Order order);

	public List<Order> orders(int cid);

	List<Order> getNewOrders(String skill);

	String acceptOrder(int oid,int pid);

	List<ProxyAcceptedOrder> acceptedOrders(int pid);

	String orderCompleted(int oid);

	String cancelOrder(int oid);
}
