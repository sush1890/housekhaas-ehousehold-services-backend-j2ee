package com.app.dao;

import java.util.List;

import com.app.pojos.Customer;
import com.app.pojos.Feedback;

public interface ICustomerDao {

	String registerCustomer(Customer c);

	String validateCustomer(String email, String password);

	Customer getDetails(int id);

	List<Customer> getCustomerList();

	String deleteCustomer(int id);

	String submitFeedback(Feedback f);

	String updateUser(Customer c);

}
