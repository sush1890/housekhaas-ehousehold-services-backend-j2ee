package com.app.dao;

import com.app.pojos.Professional;
import com.app.pojos.Skill;
import java.util.*;

public interface IProfessionalDao {

	String registerProfessional(Professional p);

	List<Skill> getSkill();

	String validateProfessional(String email, String password);

	List<Professional> getProfessionalList();

	Professional getDetails(int id);

	String deleteProfessional(int id);

	String updateProfessional(Professional p);
}
