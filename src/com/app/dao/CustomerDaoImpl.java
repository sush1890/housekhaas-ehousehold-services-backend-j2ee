package com.app.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Customer;
import com.app.pojos.Feedback;

@Repository
public class CustomerDaoImpl implements ICustomerDao{
	
	@Autowired
	private SessionFactory sf;
	
	public CustomerDaoImpl() {
		System.out.println("in constr of "+getClass().getName());
	}

	@Override
	public String registerCustomer(Customer c) {
		sf.getCurrentSession().save(c);
		return "Successful";
	}

	@Override
	public String validateCustomer(String email, String password) {
		String jpql = "select c from Customer c where c.email=:email and c.password=:password";
		Customer c = sf.getCurrentSession().createQuery(jpql, Customer.class).setParameter("email", email).setParameter("password", password).getSingleResult();
		
		if(c != null) {
			return ""+c.getCid();
		}
		return "invalid";
	}

	@Override
	public Customer getDetails(int id) {
		String jpql = "select c from Customer c where c.cid=:id";
		return sf.getCurrentSession().createQuery(jpql,Customer.class).setParameter("id", id).getSingleResult();
	}

	@Override
	public List<Customer> getCustomerList() {
		String jpql = "select c from Customer c";
		return sf.getCurrentSession().createQuery(jpql, Customer.class).getResultList();
	}

	@Override
	public String deleteCustomer(int id) {
		Session hs = sf.getCurrentSession();
		Customer c = hs.get(Customer.class, id);
		if (c != null) {
			hs.delete(c);
			return "Customer with ID " + id + " deleted";
		} else 
			return "Customer deletion failed";
	}

	@Override
	public String submitFeedback(Feedback f) {
		sf.getCurrentSession().save(f);
		return "Successful";
	}

	@Override
	public String updateUser(Customer c) {
		sf.getCurrentSession().update(c);
		return "success";
	}

}
