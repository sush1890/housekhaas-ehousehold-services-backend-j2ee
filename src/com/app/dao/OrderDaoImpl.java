package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Customer;
import com.app.pojos.Order;
import com.app.pojos.Professional;
import com.app.pojos.ProxyAcceptedOrder;

@Repository
public class OrderDaoImpl implements IOrderDao{

	@Autowired
	private SessionFactory sf;
	
	public OrderDaoImpl() {
		System.out.println("in constr of "+getClass().getName());
	}

	@Override
	public String bookOrder(int cid, Order o) {
		System.out.println("Inside book order dao");
		Customer c = sf.getCurrentSession().get(Customer.class, cid);
		c.addCustomer(o);
		sf.getCurrentSession().save(o);
		String email = c.getEmail();
		return email;
	}
	
	@Override
	public List<Order> orders(int cid) {
		System.out.println("inside order dao");
		String sql = "select o.oid, o.service, o.issue, o.order_date, o.expect_date, o.status, o.amount, p.name, p.mobile_no from Orders o left join Freelancer_Info p on p.pid = o.pid where o.cid ="+cid;
		List<Order> orderList = sf.getCurrentSession().createSQLQuery(sql).getResultList();
		return orderList;
	}

	@Override
	public List<Order> getNewOrders(String skill) {
		String jpql = "select o from Order o where service=:skill and status='New'";
		List<Order> orderList = sf.getCurrentSession().createQuery(jpql, Order.class).setParameter("skill", skill).getResultList();
		return orderList;
	}

	@Override
	public String acceptOrder(int oid, int pid) {
		Order o = sf.getCurrentSession().get(Order.class, oid);
		o.setStatus("Pending");
		Professional p = sf.getCurrentSession().get(Professional.class, pid);
		p.addProfessional(o);
		sf.getCurrentSession().update(o);
		return "Sucess";
	}

	@Override
	public List<ProxyAcceptedOrder> acceptedOrders(int pid) {
		String sql = "select o.oid, o.issue, o.expect_date, o.expect_time, o.address, o.status, o.amount, c.name, c.mobile_no, c.locality, c.zip from  Orders o inner join Customer_Info c on c.cid = o.cid where o.pid = "+pid;
		List<ProxyAcceptedOrder> orderList =  sf.getCurrentSession().createSQLQuery(sql).getResultList();
		System.out.println("Inside order Dao" + orderList);
		return orderList;
	}

	@Override
	public String orderCompleted(int oid) {
		Order o = sf.getCurrentSession().get(Order.class, oid);
		o.setStatus("Completed");
		sf.getCurrentSession().save(o);
		return "Success";
	}

	@Override
	public String cancelOrder(int oid) {
		Order o = new Order();
		o.setOid(oid);
		System.out.println(o);
		sf.getCurrentSession().delete(o);
		return "Order Cancelled";
	}

}
