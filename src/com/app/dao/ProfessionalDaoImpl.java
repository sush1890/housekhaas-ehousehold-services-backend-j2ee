package com.app.dao;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Professional;
import com.app.pojos.Skill;

@Repository
public class ProfessionalDaoImpl implements IProfessionalDao {

	@Autowired
	private SessionFactory sf;
	
	public ProfessionalDaoImpl() {
		
		System.out.println("in constr of "+getClass().getName());
	}

	@Override
	public String registerProfessional(Professional p) {
		System.out.println("Inside prof dao"+p);
		sf.getCurrentSession().save(p);
		return "Successful";
	}

	@Override
	public List<Skill> getSkill() {
	
		String jpql = "select s from Skill s";
		return sf.getCurrentSession().createQuery(jpql, Skill.class).getResultList();
	}

	@Override
	public String validateProfessional(String email, String password) {
		String jpql = "select p from Professional p where p.email=:email and p.password=:password and p.verificationStatus=true";
		Professional p = sf.getCurrentSession().createQuery(jpql, Professional.class).setParameter("email", email).setParameter("password", password).getSingleResult();
		
		if(p != null) {
			return ""+p.getPid();
		}
		return "invalid";
	}

	@Override
	public List<Professional> getProfessionalList() {
		String jpql = "select p from Professional p";
		return sf.getCurrentSession().createQuery(jpql, Professional.class).getResultList();
	}

	@Override
	public Professional getDetails(int id) {
		String jpql = "select p from Professional p where pid=:id";
		return sf.getCurrentSession().createQuery(jpql,Professional.class).setParameter("id", id).getSingleResult();
	}

	@Override
	public String deleteProfessional(int id) {
		Session hs = sf.getCurrentSession();
		Professional p = hs.get(Professional.class, id);
		if (p != null) {
			hs.delete(p);
			return "Professional with ID " + id + " deleted";
		} else 
			return "Professional deletion failed";
	}

	@Override
	public String updateProfessional(Professional p) {
		sf.getCurrentSession().update(p);
		return "success";
	}
	
}
