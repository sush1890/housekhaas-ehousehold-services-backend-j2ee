package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Admin;
import com.app.pojos.Feedback;
import com.app.pojos.Order;
import com.app.pojos.Professional;
import com.app.pojos.Skill;

@Repository
public class AdminDaoImpl implements IAdminDao {

	@Autowired
	private SessionFactory sf;
	
	public AdminDaoImpl() {
		System.out.println("in constr of "+getClass().getName());
	}
	
	@Override
	public String validateAdmin(String email, String password) {
		String jpql = "select a from Admin a where a.email=:email and a.password=:password";
		Admin a = sf.getCurrentSession().createQuery(jpql, Admin.class).setParameter("email", email).setParameter("password", password).getSingleResult();
		
		if(a != null) {
			return ""+a.getAid();
		}
		return "invalid";
	}

	@Override
	public String addService(Skill s) {
		sf.getCurrentSession().save(s);
		return "Successful";
	}

	@Override
	public List<Feedback> getFeedbackList() {
		String jpql = "select f from Feedback f";
		return sf.getCurrentSession().createQuery(jpql, Feedback.class).getResultList();
	}

	@Override
	public List<Order> getAllOrders() {
		String jpql = "select o from Order o";
		return sf.getCurrentSession().createQuery(jpql, Order.class).getResultList();
	}

	@Override
	public String verifyProfessional(int id) {
		System.out.println("in verify pro dao");
		Professional p = sf.getCurrentSession().get(Professional.class, id);
		p.setVerificationStatus(true);
		return "Success";
	}

}
