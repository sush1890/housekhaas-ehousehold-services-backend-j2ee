package com.app.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.pojos.Order;
import com.app.pojos.ProxyAcceptedOrder;
import com.app.pojos.ProxyOrder;
import com.app.pojos.ProxyProfessional;
import com.app.service.IOrderService;

@CrossOrigin(origins = "http://localhost:4200",allowedHeaders="*")
@Controller
@RequestMapping("/orders")
public class OrderController {
	
	@Autowired
	private IOrderService service;
	@Autowired
	private JavaMailSender sender;
	
	public OrderController() {
		System.out.println("in cnstr " + getClass().getName());
	}
	
	@PostMapping("/place-order")
	public ResponseEntity<String> bookOrder(@RequestBody ProxyOrder o){
		System.out.println("in add order "+o);
		try {
			int cid = o.getCid();
			java.util.Date date = new java.util.Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String d = format.format(date);
			Order order = new Order(o.getAddress(), 150.00, o.getExpectDate(), o.getExpectTime(), o.getIssue(), d, "Pending", o.getMobileNo(),  o.getService(), "New");

			String msg = service.bookOrder(cid,order);
			if(msg != null) {
				SimpleMailMessage mesg=new SimpleMailMessage();
				mesg.setTo(msg);
				mesg.setSubject("HouseKhaas Order Placed");
				mesg.setText("Thank you for using HouseKhaas. Your order has been placed, we will contact you once our serviceperson accepts the order.");
				sender.send(mesg);
			}
			return new ResponseEntity<String>(msg, HttpStatus.OK);
		} catch(RuntimeException e) {
			return new ResponseEntity<String>("Order Addition failed"+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("userOrderList/{id}")
	public ResponseEntity<List<Order>> getDetails(@PathVariable int id){
		System.out.println("in orders details ");
		List<Order> orders = service.orders(id);
		System.out.println(orders);
		return new ResponseEntity<List<Order>>(orders, HttpStatus.OK);
	}
	
	@PostMapping("/newOrders")
	public ResponseEntity<List<Order>> getNewOrders(@RequestBody ProxyProfessional p){
		String skill = p.getSkill();
		System.out.println("inside orders " + skill);
		List<Order> orders = service.getNewOrders(skill);
		System.out.println(orders);
		return new ResponseEntity<List<Order>>(orders, HttpStatus.OK);
	}
	
	@PostMapping("/acceptOrder")
	public ResponseEntity<String> acceptOrder(@RequestBody ProxyOrder o){
		System.out.println("inside accept" + o);
		int oid = o.getOid();
		int pid = o.getPid();
		System.out.println("inside accept" + oid);
		String msg = service.acceptOrder(oid,pid);
		System.out.println(msg);
		return new ResponseEntity<String>(msg, HttpStatus.OK);
	}
	
	@PostMapping("/acceptedOrders")
	public ResponseEntity<List<ProxyAcceptedOrder> > acceptedOrders(@RequestBody ProxyOrder o){
		System.out.println("inside accept" + o);
		int pid = o.getPid();
		List<ProxyAcceptedOrder>  orderList = service.acceptedOrders(pid);
		System.out.println(orderList);
		return new ResponseEntity<List<ProxyAcceptedOrder>>(orderList, HttpStatus.OK);
	}
	
	@PostMapping("/orderComplete")
	public ResponseEntity<String> orderCompleted(@RequestBody ProxyOrder o){
		System.out.println("inside accept" + o);
		int oid = o.getOid();
		System.out.println("inside accept" + oid);
		String msg = service.orderCompleted(oid);
		System.out.println(msg);
		return new ResponseEntity<String>(msg, HttpStatus.OK);
	}

	
	@PostMapping("/cancelOrder")
	public ResponseEntity<String> cancelAnOrder(@RequestBody ProxyOrder o){
		System.out.println("inside accept" + o);
		int oid = o.getOid();
		System.out.println("inside accept" + oid);
		String msg = service.cancelOrder(oid);
		System.out.println(msg);
		return new ResponseEntity<String>(msg, HttpStatus.OK);
	}
}
