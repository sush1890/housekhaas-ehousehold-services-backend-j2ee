package com.app.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.app.pojos.Professional;
import com.app.pojos.ProxyProfessional;
import com.app.pojos.Skill;
import com.app.service.IProfessionalService;

@CrossOrigin(origins = "http://localhost:4200",allowedHeaders="*")
@Controller
@RequestMapping("/professional")
public class ProfessionalController {
	
	@Autowired
	private IProfessionalService service;

	public ProfessionalController() {
		System.out.println("in cnstr " + getClass().getName());
	}
	
	@PostMapping("/register")
	public ResponseEntity<String> registerCustomer(
			@RequestParam String name,
			@RequestParam String email, 
			@RequestParam String password, 
			@RequestParam String mobileNo, 
			@RequestParam String address,
			@RequestParam String zip, 
			@RequestParam String locality, 
			@RequestParam String city,
			@RequestParam String state,
			@RequestParam String skill,
			@RequestParam MultipartFile document){
		
		System.out.println("in add professional " + name);
		Professional p = new Professional(name, email, password, mobileNo, address, zip, locality, city, state, skill);
		try {
			p.setDocument(document.getBytes());
			service.registerProfessional(p);
			return new ResponseEntity<String>("Professional added", HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@GetMapping("/skill")
	public ResponseEntity<List<Skill>> getSkill(){
		System.out.println("in get skill ");
		try {
			return new ResponseEntity<List<Skill>>(service.getSkills(), HttpStatus.OK);
		}
		catch (RuntimeException e) {
			return null;
		}	
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> processLoginForm(@RequestBody ProxyProfessional pp) {
		System.out.println("in process login form " + pp.getEmail() + " " + pp.getPassword());
		try {
				return new ResponseEntity<String>(service.validateProfessional(pp.getEmail(), pp.getPassword()), HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<String>("login failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("getDetails/{id}")
	public ResponseEntity<Professional> getDetails(@PathVariable int id){
		System.out.println("in get details of professional");
		return new ResponseEntity<Professional>(service.getDetails(id), HttpStatus.OK);
	}
	
	@GetMapping("/list")
	public ResponseEntity<List<Professional>> getCustomerList() {
		System.out.println("in get list");
		List<Professional> plist = service.getProfessionalList();
		System.out.println(plist);
		return new ResponseEntity<List<Professional>>(plist, HttpStatus.OK);
	}
	
	@DeleteMapping("delete/{id}")
	public ResponseEntity<String> deleteProfessional(@PathVariable int id)
	{
		System.out.println("in delete movie "+id);
		return new ResponseEntity<String>(service.deleteProfessional(id),HttpStatus.OK);
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updateUser(Professional p){
		System.out.println("in update details of professional "+p);
		return new ResponseEntity<String>(service.updateProfessional(p), HttpStatus.OK);
	}
}
